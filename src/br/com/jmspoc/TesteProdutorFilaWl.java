package br.com.jmspoc;

import javax.jms.JMSException;
import javax.naming.NamingException;

public class TesteProdutorFilaWl {

	public static void main(String[] args) throws Exception {

		// create an object to invoke initialize(), send() and close() methods
		JMSQueueProducer jmsQueueProducer = null;
		//String enqueueMessage = "<pedido><id>4566</id></pedido>";
		String enqueueMessage = "quit";

		try {
			
			jmsQueueProducer = new JMSQueueProducer();
			jmsQueueProducer.send(enqueueMessage); // invokes send method with actual message
			jmsQueueProducer.closeConnParams(); // invokes to close all connections

			System.out.println("JMSQueueProducer: Text message \"" + enqueueMessage
					+ "\" enqueued to JMS Queue 'TestQueue' successfully ");
		} catch (NamingException nex) {
			nex.printStackTrace();
		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
		}

	}

}
