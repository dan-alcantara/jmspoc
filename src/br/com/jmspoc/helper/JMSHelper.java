package br.com.jmspoc.helper;

import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JMSHelper {
	// JNDI name for Weblogic, ConnectionFactory & Queue
    private static final String WEBLOGIC_JMS_URL = "t3://10.122.50.36:8901"; // Weblogic JMS URL
    public final static String WEBLOGIC_JNDI_FACTORY_NAME = "weblogic.jndi.WLInitialContextFactory"; // Weblogic JNDI
    private static final String CONNECTION_FACTORY_JNDI_NAME = "jms/ExtracaoExecucaoIntegracaoFactory"; // Weblogic ConnectionFactory JNDI
    private static final String QUEUE_JNDI_NAME = "jms/ExtracaoExecucaoIntegracaoQueue"; // Weblogic Queue JNDI
    //private static final String SECURITY_PRINCIPAL = "weblogic";
    //private static final String SECURITY_CREDENTIALS = "admin123";
 
    // variables 
    private Hashtable<String, String> wlsEnvParamHashTbl = null;
    private static InitialContext initialContext = null;
    private static QueueConnectionFactory queueConnectionFactory = null;
    private static QueueConnection queueConnection = null;
    public static QueueSession queueSession = null;
    public static Queue queue = null;
 
    /**
     * default constructor
     * @throws JMSException 
     * @throws NamingException 
     */
    public JMSHelper() throws NamingException, JMSException {
 
        wlsEnvParamHashTbl = new Hashtable<String, String>();
        wlsEnvParamHashTbl.put(Context.PROVIDER_URL, WEBLOGIC_JMS_URL); // set Weblogic JMS URL
        wlsEnvParamHashTbl.put(Context.INITIAL_CONTEXT_FACTORY, WEBLOGIC_JNDI_FACTORY_NAME); // set Weblogic JNDI
        //wlsEnvParamHashTbl.put(Context.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL); // set Weblogic UserName
        //wlsEnvParamHashTbl.put(Context.SECURITY_CREDENTIALS, SECURITY_CREDENTIALS); // set Weblogic PassWord
        
        initializeConnParams();
        
    }
 
    /**
     * This method initializes all necessary connection parameters for establishing JMS Queue communication
     * 
     * @throws NamingException
     * @throws JMSException
     */
    public void initializeConnParams() throws NamingException, JMSException {
 
        initialContext = new InitialContext(wlsEnvParamHashTbl); // set InitialContext 
        queueConnectionFactory = (QueueConnectionFactory) initialContext.lookup(CONNECTION_FACTORY_JNDI_NAME); // lookup using initial context
        queueConnection = queueConnectionFactory.createQueueConnection(); // create ConnectionFactory
        queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // create QueueSession
        queue = (Queue) initialContext.lookup(QUEUE_JNDI_NAME); // lookup Queue JNDI using initial context created above
        queueConnection.start(); // start Queue connection
        
    }
    
    public TextMessage createTextMessage() throws JMSException {
    	return queueSession.createTextMessage();
    }
    
    public QueueSender createSender() throws JMSException {
    	return queueSession.createSender(queue);
    }
    
    public QueueReceiver createReceiver() throws JMSException {
    	return queueSession.createReceiver(queue);
    }
    
    public void closeConnParams() throws JMSException {
    	 
        queueSession.close();
        queueConnection.close();
        
    }
    
}
