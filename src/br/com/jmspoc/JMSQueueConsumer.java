package br.com.jmspoc;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.QueueReceiver;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import br.com.jmspoc.helper.JMSHelper;

public class JMSQueueConsumer implements MessageListener {

	private static QueueReceiver queueReceiver = null;
	private static TextMessage textMessage = null;
	private boolean quit = false;
	private JMSHelper jmsHelper = null;

	/**
	 * default constructor
	 * @throws JMSException 
	 * @throws NamingException 
	 */
	public JMSQueueConsumer() throws NamingException, JMSException {
		jmsHelper = new JMSHelper();
		queueReceiver = jmsHelper.createReceiver();
		queueReceiver.setMessageListener(this);
	}

	/**
	 * onMessage() listener from MessageListener class to read messages
	 */
	@Override
	public void onMessage(Message message) {

		String consumedMessagefromQueue = null;

		try {
			if (message instanceof TextMessage) { // It's TextMessage...

				textMessage = (TextMessage) message;
				consumedMessagefromQueue = textMessage.getText();
				
			} else { // If it is not a TextMessage...

				consumedMessagefromQueue = message.toString();
				
			}

			// finally print the message to the output console
			System.out.println("JMSQueueConsumer: Message consumed from JMS Queue 'TestQueue' is >>>"
					+ consumedMessagefromQueue + "<<<");

			if (consumedMessagefromQueue.equalsIgnoreCase("quit")) {
				synchronized (this) {
					setQuit(true);
					this.notifyAll(); // Notify main thread to quit
				}
			}
		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
		}
	}

	/**
	 * This method closes all connections
	 * 
	 * @throws JMSException
	 */
	public void close() throws JMSException {

		queueReceiver.close();
		jmsHelper.closeConnParams();
		
	}

	public boolean isQuit() {
		return quit;
	}

	public void setQuit(boolean quit) {
		this.quit = quit;
	}
}
