package br.com.jmspoc;

import javax.jms.JMSException;
import javax.naming.NamingException;

public class TesteConsumidorFilaWl {

	public static void main(String[] args) throws Exception {

		// create an object to invoke initialize(), onMessage() and close() methods
		JMSQueueConsumer jmsQueueConsumer = null;

		try {

			jmsQueueConsumer = new JMSQueueConsumer();

			synchronized (jmsQueueConsumer) {
				while (!jmsQueueConsumer.isQuit()) {
					try {
						jmsQueueConsumer.wait();
					} catch (InterruptedException ie) {
					}
				}
			}

			// to invoke close() method to close all established connections
			jmsQueueConsumer.close();
		} catch (NamingException nex) {
			nex.printStackTrace();
		} catch (JMSException jmsex) {
			jmsex.printStackTrace();
		}

	}

}
