package br.com.jmspoc;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;

public class TesteProdutorFila {

	public static void main(String[] args) throws Exception {

		InitialContext context = new InitialContext();

		ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
		Connection connection = connectionFactory.createConnection();
		connection.start();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = (Destination) context.lookup("financeiro");
		MessageProducer producer = session.createProducer(destination);
		
		for (int index = 1; index < 1000; index++) {
			Message message = session.createTextMessage("<pedido><id>" + index + "</id></pedido>");
			//producer.send(message, DeliveryMode.NON_PERSISTENT, 3, 5000);
			producer.send(message);
		}
		
		// Priorização de mensagens
		/*Message message1 = session.createTextMessage("ERROR | ....");
		producer.send(message1, DeliveryMode.NON_PERSISTENT, 9, 5000000);*/

		session.close();
		connection.close();
		context.close();

	}

}
