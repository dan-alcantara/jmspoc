package br.com.jmspoc;

import javax.jms.JMSException;
import javax.jms.QueueSender;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import br.com.jmspoc.helper.JMSHelper;

public class JMSQueueProducer {
	

	private static QueueSender queueSender = null;
    private static TextMessage textMessage = null;
    private JMSHelper jmsHelper = null;
 
    public JMSQueueProducer() throws NamingException, JMSException {
    	super();
    	jmsHelper = new JMSHelper();
        textMessage = jmsHelper.createTextMessage(); // create TextMessage for Queue
        queueSender = jmsHelper.createSender();
    }
 
    /**
     * This is the actual method to SEND messages to JMS Queues
     * 
     * @param message
     * @throws JMSException
     */
    public void send(String message) throws JMSException {
 
        textMessage.setText(message);
        queueSender.send(textMessage);
        
    }
 
    /**
     * This method closes all connections
     * 
     * @throws JMSException
     */
    public void closeConnParams() throws JMSException {
    	
    	jmsHelper.closeConnParams();
        queueSender.close();
        
    }
}
