package br.com.jmspoc;

import java.io.StringWriter;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.xml.bind.JAXB;

import br.com.jmspoc.modelo.Pedido;
import br.com.jmspoc.modelo.PedidoFactory;

public class TesteProdutorTopico {

	public static void main(String[] args) throws Exception {

		InitialContext context = new InitialContext();

		ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
		Connection connection = connectionFactory.createConnection();
		connection.start();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination destination = (Destination) context.lookup("loja");
		MessageProducer producer = session.createProducer(destination);
		Message message = null;
		
		for (int index = 1; index <= 2; index++) {
			Pedido pedido = new PedidoFactory().geraPedidoComValores();
			pedido.setCodigo(index);
			StringWriter stringWriter = new StringWriter();
			JAXB.marshal(pedido, stringWriter);
			message = session.createTextMessage(stringWriter.toString());
			message.setStringProperty("name", "Daniel Cordeiro");
			producer.send(message);
		}
		
		message = session.createTextMessage("<pedido><id>999</id></pedido>");
		producer.send(message);

		session.close();
		connection.close();
		context.close();

	}

}
